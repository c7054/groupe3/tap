variable "subsid" {
  type        = string
  default     = "36a3208f-9672-41cc-b490-a793fc92210d"
  description = "identifiant abonnement"
}

variable "tenantid" {
  type        = string
  default     = "190ce420-b157-44ae-bc2f-69563baa5a3b"
  description = "id du tenant"
}

variable "resource_group_name" {
  type        = string
  default     = "rscterra"
  description = "nom du groupe du ressource"
}
variable "resource_group_location" {
  type        = string
  default     = "eastus"
  description = "region du ressource group"
}
variable "image" {
  type        = string
  default     = "Img_Debian11_template"
  description = "nom de limage"
}
