# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.98.0"
    }
  }

  required_version = ">= 1.1.0"
}

provider "azurerm" {
  features {}
  subscription_id = "36a3208f-9672-41cc-b490-a793fc92210d"
  tenant_id       = "190ce420-b157-44ae-bc2f-69563baa5a3b"
}
data "azurerm_resource_group" "main" {
  name = "rscterra"
}

data "azurerm_image" "main" {
  name                = "Img_Debian11_template"
  resource_group_name = data.azurerm_resource_group.main.name
}
resource "azurerm_network_security_group" "main" {
  name                = "acceptanceAllRules"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name

  security_rule {
    name                       = "AllowAll"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_public_ip" "main" {
  name                = "Linux-PublicIp"
  resource_group_name = data.azurerm_resource_group.main.name
  location            = data.azurerm_resource_group.main.location
  allocation_method   = "Static"

  provisioner "local-exec" {
    command = "echo ${self.ip_address} > inventory"
  }
}

resource "azurerm_virtual_network" "main" {
  name                = "Linux-network"
  address_space       = ["10.0.0.0/16"]
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
}

resource "azurerm_subnet" "main" {
  name                 = "internal"
  resource_group_name  = data.azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "main" {
  name                = "Linux-nic"
  resource_group_name = data.azurerm_resource_group.main.name
  location            = data.azurerm_resource_group.main.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.main.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.main.id
  }
}

resource "azurerm_linux_virtual_machine" "Wordpress" {
  name                            = "Linux_WP"
  computer_name                   = "wordpress.labo.fr"
  resource_group_name             = data.azurerm_resource_group.main.name
  location                        = data.azurerm_resource_group.main.location
  size                            = "Standard_B1ms"
  admin_username                  = "adminuser"
  custom_data                     = base64encode("Hello World!")
  disable_password_authentication = true
  network_interface_ids = [
    azurerm_network_interface.main.id,
  ]

  source_image_id = data.azurerm_image.main.id

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("~/.ssh/id_rsa.pub")
  }
}

output "public_adresses_linux_vm" {
  description = "outputed new ip addr"
  value       = azurerm_linux_virtual_machine.Wordpress.public_ip_address
}
resource "null_resource" "create_inventory" {
  provisioner "local-exec" {
    command     = "ansible-playbook ../03_BUILD/playbook/wordpress.yml -i inventory"
    interpreter = ["/bin/bash", "-c"]
  }
  depends_on = [azurerm_linux_virtual_machine.Wordpress]
}
