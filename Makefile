TMPFOLDER = tmp
PACKERFILE = 01_IMAGE/debian_template.pkr.hcl
TERRA00 = terraform -chdir=00_INIT
TERRA02 = terraform -chdir=02_DEPLOY

start : deploy

deploy : image
		$(TERRA02) init
		$(TERRA02) apply -auto-approve


image : init
		packer init $(PACKERFILE)
		packer build -force $(PACKERFILE)

init :
		$(TERRA00) init
		$(TERRA00) plan
		$(TERRA00) apply -auto-approve

stop :
		echo yes | $(TERRA00) destroy
